# Owlstone Medical Exercise

This project is an exercise for Owlstone Medical. It involves the analysis of the text with the purpose of extracting statistics from the text.

It is a web application built using the Django Framework. It analyses text and extracts the named entities and commonly mentioned words using the NLTK package.

## Usage

Enter this address in your web browser:

```
http://165.227.234.2/app/index
```