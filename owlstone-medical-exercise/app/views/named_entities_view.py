from django.views import View
from django.shortcuts import render

from ..controllers import DataController


class NamedEntitiesView(View):

    def get(self, request):
        """ Get named entities page

        :return render html page
        """
        return render(
            request, 
            'named_entities.html',
            {'data': DataController().getNamedEntities()}
        )