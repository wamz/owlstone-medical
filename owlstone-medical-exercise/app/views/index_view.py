from django.views import View
from django.shortcuts import render

from ..controllers import DataController


class IndexView(View):

    def get(self, request):
        """ Get index page

        :return render html page
        """
        return render(
            request, 
            'index.html',
            {'text': DataController().getFileText()}
        )