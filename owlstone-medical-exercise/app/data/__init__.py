import os

def openTextFile():
    """ Open text file
    
    :return string
    """
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    with open(os.path.join(BASE_DIR, 'data/random_text.txt')) as f:
        return f.readlines()