import nltk
from nltk.tokenize import word_tokenize


class Paragraph:

    def __init__(self, text):
        """ Constructor

        :return void
        """
        self.words = nltk.word_tokenize(text)

    def __str__(self):
        """ Return paragraph text

        :return string
        """
        return ' '.join(self.words)

    def getTaggedWords(self):
        """ POS tag words in the paragraph

        :return list
        """
        return nltk.pos_tag(self.words)

    def getNamedEntities(self):
        """ Get named entities identitfied in the 
        paragraph

        :return list
        """
        namedEntities = []
        for entity in nltk.ne_chunk(self.getTaggedWords(), binary=True):
            if isinstance(entity, nltk.tree.Tree):
                namedEntities.append(" ".join([token for token, pos in entity.leaves()]))

        return namedEntities