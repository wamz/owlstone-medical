import nltk
from .. import data
from .paragraph import Paragraph


class File:
    
    def __init__(self):
        """ Constructor

        :return void
        """
        self.file = data.openTextFile()
        self.paragraphs = []

    def getText(self):
        """ Return text extracted from file

        :return list
        """
        return self.file

    def getParagraphs(self):
        """ Extract paragraphs from text file

        :return list
        """
        for paragraph in self.file:
            self.paragraphs.append(Paragraph(paragraph))

        return self.paragraphs

    def getCommonWords(self):
        """ Extract the most commonly used words
        from the text

        :return list
        """
        words = []
        for line in self.file:
            if '\n' not in line.split(' '):
                for word in line.split(' '):
                    words.append(word)

        return nltk.FreqDist(words).most_common(20)